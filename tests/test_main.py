#!/usr/bin/env python3
# vim: set tw=100 cc=+1:
# pylint: disable=bad-continuation,missing-docstring
# pylint: disable=unused-variable,unused-import

import logging
import sys

import pytest
import xadix.argparse_tree as apt

def test_bad_option():
    with pytest.raises(TypeError) as excinfo:
        apn = apt.ArgParseNode(options={"invalid_option": True})
    assert "unexpected keyword argument" in str(excinfo.value)

def test_add_subparsers_required():
    if (sys.version_info.major == 3) and (sys.version_info.minor == 6) and \
        sys.version_info.micro <= 10:
        logging.warning(("Not testing add_subparsers with required=... "
            "as this does not work on this version of python"))
        return
    root = apt.ArgParseNode(options={"add_help": True}, subparser_options={"required": True})
    current = root.get_path("id")
    current = root.get_path("id/list")
    with pytest.raises(SystemExit) as excinfo:
        root.parser.parse_args(["id"])
    parsed = root.parser.parse_args(["id", "list"])
    assert root.get_selected_path(parsed) == "id/list"

def test_foo():
    root = apt.ArgParseNode(options={"add_help": True, "description": "argparse_tree example"})
    assert root is not None
    assert isinstance(root, apt.ArgParseNode)
    node_c = root.get("a").get("b").get("c")
    node_d = root.get("a").get("b").get("d")
    node_lone = root.get("lone")

    assert node_c is root.get("a").get("b").get("c")
    assert node_c is root.get_path("a/b/c")
    assert node_d is root.get("a").get("b").get("d")
    assert node_d is root.get_path("a/b/d")
    assert node_lone is root.get("lone")
    assert node_lone is root.get_path("lone")
    assert node_c != node_d
    assert node_c.level == 3
    assert node_lone.level == 1
    assert root.level == 0
    assert root.get("single").path == "single"
    assert root.get("single").path_list == ["single"]

    parsed = root.parser.parse_args(["a", "b", "c"])
    assert parsed.subparser_0 == "a"
    assert parsed.subparser_1 == "b"
    assert parsed.subparser_2 == "c"
    assert root.get_selected_path_list(parsed) == ["a", "b", "c"]
    assert root.get_selected_path(parsed) == "a/b/c"
    assert root.get_selected_node(parsed) is node_c

    assert root.get("a").get("b").get("c").path == "a/b/c"
    assert root.get("a").get("b").get("c").path_list == ["a", "b", "c"]

    with pytest.raises(SystemExit) as excinfo:
        parsed = root.parser.parse_args(["a", "b", "XXX"])
    #assert "invalid choice" in str(excinfo.value)

    parsed = root.parser.parse_args([])
    assert apt.ArgParseNode.get_selected_path_list(parsed) == []
    assert root.get_selected_path_list(parsed) == []
    assert apt.ArgParseNode.get_selected_path(parsed) == ""
    assert root.get_selected_path(parsed) == ""
    assert root.get_selected_node(parsed) is root

    parsed = root.parser.parse_args(["a"])
    assert apt.ArgParseNode.get_selected_path_list(parsed) == ["a"]
    assert apt.ArgParseNode.get_selected_path(parsed) == "a"

    parsed = root.parser.parse_args(["a", "b"])
    assert apt.ArgParseNode.get_selected_path_list(parsed) == ["a", "b"]
    assert apt.ArgParseNode.get_selected_path(parsed) == "a/b"

    parsed = root.parser.parse_args(["a", "b", "d"])
    assert root.get_selected_path_list(parsed) == ["a", "b", "d"]
    assert root.get_selected_path(parsed) == "a/b/d"
    assert root.get_selected_node(parsed) is node_d
