#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=100 cc=+1:
# pylint: disable=bad-continuation,missing-docstring

import sys
import xadix.argparse_tree as apt

def main():
    # pylint: disable=unused-variable
    root = apt.ArgParseNode(options={"add_help": True, "description": "argparse_tree example"},
        default_options={"add_help": True})
    root.parser.add_argument("--version",
        action="version", version="???")
    root.parser.add_argument("-v", "--verbose",
        action="count", dest="verbosity",
        help="increase verbosity level")
    root.parser.add_argument("-s", "--some",
        action="store", dest="some", type=str, required=False, default=None,
        help="???")
    node_git = root.get("git")
    node_git.parser.add_argument("-C",
        action="store", dest="git_path", type=str, required=False, default=None,
        help="...")
    node_git_id = root.get("git").get("id")
    node_git_id.parser.add_argument("--file",
        action="store", dest="git_ids_file", type=str, required=False,
        help="...")
    node_git_id_list = root.get("git").get("id").get("list")
    node_git_id_set = root.get("git").get("id").get("set")
    node_git_id_fromlog = root.get("git").get("id").get("fromlog")

    parse_result = root.parser.parse_args(args=sys.argv[1:])

    print("root.get_selected_path(parse_result) = '{}'".
        format(root.get_selected_path(parse_result)))
    print("root.get_selected_path_list(parse_result) = '{}'".
        format(root.get_selected_path_list(parse_result)))
    print("root.get_selected_node(parse_result) is node_git_id_list  = '{}'".
        format(root.get_selected_node(parse_result) is node_git_id_list))


if __name__ == "__main__":
    main()
