xadix package
=============

Subpackages
-----------

.. toctree::

   xadix.argparse_tree

Module contents
---------------

.. automodule:: xadix
   :members:
   :undoc-members:
   :show-inheritance:
